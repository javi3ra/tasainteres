<%-- 
    Document   : index
    Created on : 08-abr-2020, 20:04:51
    Author     : javi3
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>calcula tu interes</title>
    </head>
    <body class="text-center" >
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">Calcular tasa de interes</h3>    
        </div>
      </header>
    </div>
      <main role="main" class="inner cover">
        <h1 class="cover-heading">Calcula!</h1>
        <p class="">
            
    <form name="form" action="controller" method="POST">     
       
         <div id="id1" class="input-group-prepend" style="margin-left: 130px;margin-right: 130px;margin-top: 50px;">    
           <span class="input-group-text" style="border-left-width: 2px;border-right-width: 2px;border-bottom-width: 2px;border-top-width: 2px;margin-left: 12px;">capital</span>
           <span class="input-group-text">$</span>
           <input type="number" name="capital" value="" class="form-control" required="">
         
           <span class="input-group-text" style="margin-left: 12px;">Tasa Interes Anual</span>
         <input type="number" name="interes" value="" class="form-control" required>        
           <span class="input-group-text" style="margin-left: 0px;">%</span>
         
           <span class="input-group-text" style="margin-left: 12px;">años</span>
         <input type="number" name="ano" value="" MIN="1" MAX="100" class="form-control" required>
      
         </div>
        <p>  
           <button type="submit" value="Calcular" class="btn btn-lg btn-secondary" style="margin-top: 50px;margin-bottom: 50px;">Calcular</button>
    </form>
        <p class="lead">
      </main>

      <footer class="mastfoot mt-auto">
        <div class="container">
          <p> Taller de aplicaciones empresariales // Fernanda Anasco</p>
          </div>
        </footer>
      
       
    </body>
</html>